package br.com.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import br.com.util.HibernateUtil;

public class GenericDAO<BEAN> {

	public Integer save(BEAN bean){
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		Integer id = null;
		try {
			transaction = session.beginTransaction();
			id = (Integer) session.save(bean);
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return id;
	}
	
	public void update(BEAN bean){
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.update(bean);
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}
	
	@SuppressWarnings("unchecked")
	public  List<BEAN> findAll(Class<BEAN> bean) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		List<BEAN> list = null;
		
		try {
			transaction = session.beginTransaction();
			list = session.createQuery("from "+bean.getName()).list();
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return list;
	}

	public void delete(BEAN bean) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		try {
			transaction = session.beginTransaction();
			session.delete(bean);
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}
	
	public static boolean insertOrUpdate(String sql, Session session) {
		org.hibernate.Transaction tx = null;
		boolean result = false;
		try {
			tx = session.beginTransaction();
			session.createSQLQuery(sql).executeUpdate();
			tx.commit();
			result = true;
		} catch (HibernateException e) {
			e.printStackTrace();
		}finally{
			session.close();
		}
		return result;
	}
}
