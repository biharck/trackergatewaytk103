package br.com.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import br.com.easytracking.bean.GPRMC;
import br.com.util.HibernateUtil;

public class GPRMCDao extends GenericDAO<GPRMC>{
	
	public  List<GPRMC> findBy(Integer next) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		List<GPRMC> list = null;
		
		try {
			transaction = session.beginTransaction();
			list = session.createQuery("from GPRMC where id > "+next).list();
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return list;
	}

}
