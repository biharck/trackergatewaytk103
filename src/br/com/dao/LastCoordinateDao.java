package br.com.dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import br.com.easytracking.bean.LastCoordinate;
import br.com.util.HibernateUtil;

public class LastCoordinateDao extends GenericDAO<LastCoordinate>{
	
	public  LastCoordinate findUnique() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction transaction = null;
		LastCoordinate lastCoordinate = null;
		
		
		try {
			transaction = session.beginTransaction();
			lastCoordinate = (LastCoordinate) session.createQuery("from LastCoordinate where id = 1").uniqueResult();
			transaction.commit();
		} catch (HibernateException e) {
			transaction.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
		return lastCoordinate;
	}

}
