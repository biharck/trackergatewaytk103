package br.com.util;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

import br.com.easytracking.bean.GPRMC;
import br.com.easytracking.bean.LastCoordinate;


public class HibernateUtil {

	private static final SessionFactory sessionFactory;

	static {
		try {
			sessionFactory = new AnnotationConfiguration()
								.configure()
								.addPackage("br.com.bean") //the fully qualified package name
								.addAnnotatedClass(GPRMC.class)
								.addAnnotatedClass(LastCoordinate.class)
								.buildSessionFactory();

		} catch (Throwable ex) {
			System.err.println("Cria��o do SessionFactory falhou:." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}
}
