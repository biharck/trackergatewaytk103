package br.com.easytracking.gateway;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.TimerTask;

import javax.xml.rpc.ServiceException;

import br.com.dao.GPRMCDao;
import br.com.dao.LastCoordinateDao;
import br.com.easytracking.bean.GPRMC;
import br.com.easytracking.bean.LastCoordinate;
import br.com.easytracking.tracker.bean.xsd.Coordenadas;
import br.com.easytracking.tracker.position.ReceiverPositionLocator;
import br.com.util.TokenUtil;

public class GateWayTK103 extends TimerTask {

	public void run() {
		GateWayTK103 gateWayTK103 = new GateWayTK103();
		gateWayTK103.updateCoordinates();
    }
	
	public void updateCoordinates(){
		//Busca ultimo ID
		LastCoordinateDao lastCoordinateDao = new LastCoordinateDao();
		LastCoordinate lastCoordinate = lastCoordinateDao.findUnique();
		Integer next = lastCoordinate.getLast();
		System.out.println(next);
		//Busca as coordenadas baseadas no ultimo ID
		GPRMCDao gprmcDao = new GPRMCDao();
		List<GPRMC> coordenadas = gprmcDao.findBy(next);
		System.out.println(coordenadas.size());

		if(coordenadas.size()==0)return;
		//Salva no banco principal as coordenadas
		Integer lastId = save(coordenadas);
		
		lastCoordinate.setLast(lastId);
		lastCoordinateDao.update(lastCoordinate);
	}
	
	public Integer save(List<GPRMC> c){
		
		List<Coordenadas> coordenadas = new ArrayList<Coordenadas>();
		
		Coordenadas coordenada = null;
		Integer last = 1;
		for (GPRMC gpr : c) {
			coordenada = new Coordenadas();
			coordenada.setDate(gpr.getDate().toString());
			coordenada.setSerial(gpr.getImei());
			coordenada.setLatitude(getLatLng(gpr.getLatitudeDecimalDegrees(),getMultiplicador(gpr.getLatitudeHemisphere())));
			coordenada.setLongitude(getLatLng(gpr.getLongitudeDecimalDegrees(),-1));;
			coordenada.setSpeed(getVelocidade(gpr.getSpeed()).toString());
			coordenadas.add(coordenada);
			last = gpr.getId();
		}
		
		Coordenadas[] coordenadasArray = new Coordenadas[coordenadas.size()];
		coordenadas.toArray(coordenadasArray);
		
		ReceiverPositionLocator locator = new ReceiverPositionLocator();
		try {
			locator.getReceiverPositionHttpSoap11Endpoint().receiver(coordenadasArray,TokenUtil.getToken());
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		
		return last;
	}
	
	public static String getLatLng(String degree, int multiplicador){
		if(degree.length()==9)
			degree = "0"+degree;
		
		float g = Float.parseFloat(degree.substring(0, 3));
		float d = Float.parseFloat(degree.substring(3, degree.length()));
		
		Float latitude = g + (d/60);
		latitude = latitude * multiplicador;
		
		return latitude.toString();
	}
	
	public static int getMultiplicador(char hemisphere){
		if(hemisphere=='S' || hemisphere == 's') return -1;
		else return 1;
	}
	
	public static Integer getVelocidade(float velocidade){
		return (int) (velocidade * 1.609f);
	}

}
