/**
 * Coordenadas.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.easytracking.tracker.bean.xsd;

public class Coordenadas  implements java.io.Serializable {
    private java.lang.String antiTheftStatus;

    private java.lang.String batteryCharging;

    private java.lang.String batteryFailure;

    private java.lang.String course;

    private java.lang.String csq;

    private java.lang.String date;

    private java.lang.String excessSpeed;

    private java.lang.String gprsConnection;

    private java.lang.String gpsAntennaDisconnected;

    private java.lang.String gpsAntennaFailure;

    private java.lang.String gpsSignal;

    private java.lang.String gpsSleep;

    private java.lang.String gsmJamming;

    private java.lang.Integer id;

    private java.lang.String input1;

    private java.lang.String latitude;

    private java.lang.String longitude;

    private java.lang.String moving;

    private java.lang.String output1;

    private java.lang.String panic;

    private java.lang.String serial;

    private java.lang.String snr;

    private java.lang.String speed;

    private java.lang.String svn;

    private java.lang.String temperature;

    private java.lang.String transmissionReason;

    public Coordenadas() {
    }

    public Coordenadas(
           java.lang.String antiTheftStatus,
           java.lang.String batteryCharging,
           java.lang.String batteryFailure,
           java.lang.String course,
           java.lang.String csq,
           java.lang.String date,
           java.lang.String excessSpeed,
           java.lang.String gprsConnection,
           java.lang.String gpsAntennaDisconnected,
           java.lang.String gpsAntennaFailure,
           java.lang.String gpsSignal,
           java.lang.String gpsSleep,
           java.lang.String gsmJamming,
           java.lang.Integer id,
           java.lang.String input1,
           java.lang.String latitude,
           java.lang.String longitude,
           java.lang.String moving,
           java.lang.String output1,
           java.lang.String panic,
           java.lang.String serial,
           java.lang.String snr,
           java.lang.String speed,
           java.lang.String svn,
           java.lang.String temperature,
           java.lang.String transmissionReason) {
           this.antiTheftStatus = antiTheftStatus;
           this.batteryCharging = batteryCharging;
           this.batteryFailure = batteryFailure;
           this.course = course;
           this.csq = csq;
           this.date = date;
           this.excessSpeed = excessSpeed;
           this.gprsConnection = gprsConnection;
           this.gpsAntennaDisconnected = gpsAntennaDisconnected;
           this.gpsAntennaFailure = gpsAntennaFailure;
           this.gpsSignal = gpsSignal;
           this.gpsSleep = gpsSleep;
           this.gsmJamming = gsmJamming;
           this.id = id;
           this.input1 = input1;
           this.latitude = latitude;
           this.longitude = longitude;
           this.moving = moving;
           this.output1 = output1;
           this.panic = panic;
           this.serial = serial;
           this.snr = snr;
           this.speed = speed;
           this.svn = svn;
           this.temperature = temperature;
           this.transmissionReason = transmissionReason;
    }


    /**
     * Gets the antiTheftStatus value for this Coordenadas.
     * 
     * @return antiTheftStatus
     */
    public java.lang.String getAntiTheftStatus() {
        return antiTheftStatus;
    }


    /**
     * Sets the antiTheftStatus value for this Coordenadas.
     * 
     * @param antiTheftStatus
     */
    public void setAntiTheftStatus(java.lang.String antiTheftStatus) {
        this.antiTheftStatus = antiTheftStatus;
    }


    /**
     * Gets the batteryCharging value for this Coordenadas.
     * 
     * @return batteryCharging
     */
    public java.lang.String getBatteryCharging() {
        return batteryCharging;
    }


    /**
     * Sets the batteryCharging value for this Coordenadas.
     * 
     * @param batteryCharging
     */
    public void setBatteryCharging(java.lang.String batteryCharging) {
        this.batteryCharging = batteryCharging;
    }


    /**
     * Gets the batteryFailure value for this Coordenadas.
     * 
     * @return batteryFailure
     */
    public java.lang.String getBatteryFailure() {
        return batteryFailure;
    }


    /**
     * Sets the batteryFailure value for this Coordenadas.
     * 
     * @param batteryFailure
     */
    public void setBatteryFailure(java.lang.String batteryFailure) {
        this.batteryFailure = batteryFailure;
    }


    /**
     * Gets the course value for this Coordenadas.
     * 
     * @return course
     */
    public java.lang.String getCourse() {
        return course;
    }


    /**
     * Sets the course value for this Coordenadas.
     * 
     * @param course
     */
    public void setCourse(java.lang.String course) {
        this.course = course;
    }


    /**
     * Gets the csq value for this Coordenadas.
     * 
     * @return csq
     */
    public java.lang.String getCsq() {
        return csq;
    }


    /**
     * Sets the csq value for this Coordenadas.
     * 
     * @param csq
     */
    public void setCsq(java.lang.String csq) {
        this.csq = csq;
    }


    /**
     * Gets the date value for this Coordenadas.
     * 
     * @return date
     */
    public java.lang.String getDate() {
        return date;
    }


    /**
     * Sets the date value for this Coordenadas.
     * 
     * @param date
     */
    public void setDate(java.lang.String date) {
        this.date = date;
    }


    /**
     * Gets the excessSpeed value for this Coordenadas.
     * 
     * @return excessSpeed
     */
    public java.lang.String getExcessSpeed() {
        return excessSpeed;
    }


    /**
     * Sets the excessSpeed value for this Coordenadas.
     * 
     * @param excessSpeed
     */
    public void setExcessSpeed(java.lang.String excessSpeed) {
        this.excessSpeed = excessSpeed;
    }


    /**
     * Gets the gprsConnection value for this Coordenadas.
     * 
     * @return gprsConnection
     */
    public java.lang.String getGprsConnection() {
        return gprsConnection;
    }


    /**
     * Sets the gprsConnection value for this Coordenadas.
     * 
     * @param gprsConnection
     */
    public void setGprsConnection(java.lang.String gprsConnection) {
        this.gprsConnection = gprsConnection;
    }


    /**
     * Gets the gpsAntennaDisconnected value for this Coordenadas.
     * 
     * @return gpsAntennaDisconnected
     */
    public java.lang.String getGpsAntennaDisconnected() {
        return gpsAntennaDisconnected;
    }


    /**
     * Sets the gpsAntennaDisconnected value for this Coordenadas.
     * 
     * @param gpsAntennaDisconnected
     */
    public void setGpsAntennaDisconnected(java.lang.String gpsAntennaDisconnected) {
        this.gpsAntennaDisconnected = gpsAntennaDisconnected;
    }


    /**
     * Gets the gpsAntennaFailure value for this Coordenadas.
     * 
     * @return gpsAntennaFailure
     */
    public java.lang.String getGpsAntennaFailure() {
        return gpsAntennaFailure;
    }


    /**
     * Sets the gpsAntennaFailure value for this Coordenadas.
     * 
     * @param gpsAntennaFailure
     */
    public void setGpsAntennaFailure(java.lang.String gpsAntennaFailure) {
        this.gpsAntennaFailure = gpsAntennaFailure;
    }


    /**
     * Gets the gpsSignal value for this Coordenadas.
     * 
     * @return gpsSignal
     */
    public java.lang.String getGpsSignal() {
        return gpsSignal;
    }


    /**
     * Sets the gpsSignal value for this Coordenadas.
     * 
     * @param gpsSignal
     */
    public void setGpsSignal(java.lang.String gpsSignal) {
        this.gpsSignal = gpsSignal;
    }


    /**
     * Gets the gpsSleep value for this Coordenadas.
     * 
     * @return gpsSleep
     */
    public java.lang.String getGpsSleep() {
        return gpsSleep;
    }


    /**
     * Sets the gpsSleep value for this Coordenadas.
     * 
     * @param gpsSleep
     */
    public void setGpsSleep(java.lang.String gpsSleep) {
        this.gpsSleep = gpsSleep;
    }


    /**
     * Gets the gsmJamming value for this Coordenadas.
     * 
     * @return gsmJamming
     */
    public java.lang.String getGsmJamming() {
        return gsmJamming;
    }


    /**
     * Sets the gsmJamming value for this Coordenadas.
     * 
     * @param gsmJamming
     */
    public void setGsmJamming(java.lang.String gsmJamming) {
        this.gsmJamming = gsmJamming;
    }


    /**
     * Gets the id value for this Coordenadas.
     * 
     * @return id
     */
    public java.lang.Integer getId() {
        return id;
    }


    /**
     * Sets the id value for this Coordenadas.
     * 
     * @param id
     */
    public void setId(java.lang.Integer id) {
        this.id = id;
    }


    /**
     * Gets the input1 value for this Coordenadas.
     * 
     * @return input1
     */
    public java.lang.String getInput1() {
        return input1;
    }


    /**
     * Sets the input1 value for this Coordenadas.
     * 
     * @param input1
     */
    public void setInput1(java.lang.String input1) {
        this.input1 = input1;
    }


    /**
     * Gets the latitude value for this Coordenadas.
     * 
     * @return latitude
     */
    public java.lang.String getLatitude() {
        return latitude;
    }


    /**
     * Sets the latitude value for this Coordenadas.
     * 
     * @param latitude
     */
    public void setLatitude(java.lang.String latitude) {
        this.latitude = latitude;
    }


    /**
     * Gets the longitude value for this Coordenadas.
     * 
     * @return longitude
     */
    public java.lang.String getLongitude() {
        return longitude;
    }


    /**
     * Sets the longitude value for this Coordenadas.
     * 
     * @param longitude
     */
    public void setLongitude(java.lang.String longitude) {
        this.longitude = longitude;
    }


    /**
     * Gets the moving value for this Coordenadas.
     * 
     * @return moving
     */
    public java.lang.String getMoving() {
        return moving;
    }


    /**
     * Sets the moving value for this Coordenadas.
     * 
     * @param moving
     */
    public void setMoving(java.lang.String moving) {
        this.moving = moving;
    }


    /**
     * Gets the output1 value for this Coordenadas.
     * 
     * @return output1
     */
    public java.lang.String getOutput1() {
        return output1;
    }


    /**
     * Sets the output1 value for this Coordenadas.
     * 
     * @param output1
     */
    public void setOutput1(java.lang.String output1) {
        this.output1 = output1;
    }


    /**
     * Gets the panic value for this Coordenadas.
     * 
     * @return panic
     */
    public java.lang.String getPanic() {
        return panic;
    }


    /**
     * Sets the panic value for this Coordenadas.
     * 
     * @param panic
     */
    public void setPanic(java.lang.String panic) {
        this.panic = panic;
    }


    /**
     * Gets the serial value for this Coordenadas.
     * 
     * @return serial
     */
    public java.lang.String getSerial() {
        return serial;
    }


    /**
     * Sets the serial value for this Coordenadas.
     * 
     * @param serial
     */
    public void setSerial(java.lang.String serial) {
        this.serial = serial;
    }


    /**
     * Gets the snr value for this Coordenadas.
     * 
     * @return snr
     */
    public java.lang.String getSnr() {
        return snr;
    }


    /**
     * Sets the snr value for this Coordenadas.
     * 
     * @param snr
     */
    public void setSnr(java.lang.String snr) {
        this.snr = snr;
    }


    /**
     * Gets the speed value for this Coordenadas.
     * 
     * @return speed
     */
    public java.lang.String getSpeed() {
        return speed;
    }


    /**
     * Sets the speed value for this Coordenadas.
     * 
     * @param speed
     */
    public void setSpeed(java.lang.String speed) {
        this.speed = speed;
    }


    /**
     * Gets the svn value for this Coordenadas.
     * 
     * @return svn
     */
    public java.lang.String getSvn() {
        return svn;
    }


    /**
     * Sets the svn value for this Coordenadas.
     * 
     * @param svn
     */
    public void setSvn(java.lang.String svn) {
        this.svn = svn;
    }


    /**
     * Gets the temperature value for this Coordenadas.
     * 
     * @return temperature
     */
    public java.lang.String getTemperature() {
        return temperature;
    }


    /**
     * Sets the temperature value for this Coordenadas.
     * 
     * @param temperature
     */
    public void setTemperature(java.lang.String temperature) {
        this.temperature = temperature;
    }


    /**
     * Gets the transmissionReason value for this Coordenadas.
     * 
     * @return transmissionReason
     */
    public java.lang.String getTransmissionReason() {
        return transmissionReason;
    }


    /**
     * Sets the transmissionReason value for this Coordenadas.
     * 
     * @param transmissionReason
     */
    public void setTransmissionReason(java.lang.String transmissionReason) {
        this.transmissionReason = transmissionReason;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Coordenadas)) return false;
        Coordenadas other = (Coordenadas) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.antiTheftStatus==null && other.getAntiTheftStatus()==null) || 
             (this.antiTheftStatus!=null &&
              this.antiTheftStatus.equals(other.getAntiTheftStatus()))) &&
            ((this.batteryCharging==null && other.getBatteryCharging()==null) || 
             (this.batteryCharging!=null &&
              this.batteryCharging.equals(other.getBatteryCharging()))) &&
            ((this.batteryFailure==null && other.getBatteryFailure()==null) || 
             (this.batteryFailure!=null &&
              this.batteryFailure.equals(other.getBatteryFailure()))) &&
            ((this.course==null && other.getCourse()==null) || 
             (this.course!=null &&
              this.course.equals(other.getCourse()))) &&
            ((this.csq==null && other.getCsq()==null) || 
             (this.csq!=null &&
              this.csq.equals(other.getCsq()))) &&
            ((this.date==null && other.getDate()==null) || 
             (this.date!=null &&
              this.date.equals(other.getDate()))) &&
            ((this.excessSpeed==null && other.getExcessSpeed()==null) || 
             (this.excessSpeed!=null &&
              this.excessSpeed.equals(other.getExcessSpeed()))) &&
            ((this.gprsConnection==null && other.getGprsConnection()==null) || 
             (this.gprsConnection!=null &&
              this.gprsConnection.equals(other.getGprsConnection()))) &&
            ((this.gpsAntennaDisconnected==null && other.getGpsAntennaDisconnected()==null) || 
             (this.gpsAntennaDisconnected!=null &&
              this.gpsAntennaDisconnected.equals(other.getGpsAntennaDisconnected()))) &&
            ((this.gpsAntennaFailure==null && other.getGpsAntennaFailure()==null) || 
             (this.gpsAntennaFailure!=null &&
              this.gpsAntennaFailure.equals(other.getGpsAntennaFailure()))) &&
            ((this.gpsSignal==null && other.getGpsSignal()==null) || 
             (this.gpsSignal!=null &&
              this.gpsSignal.equals(other.getGpsSignal()))) &&
            ((this.gpsSleep==null && other.getGpsSleep()==null) || 
             (this.gpsSleep!=null &&
              this.gpsSleep.equals(other.getGpsSleep()))) &&
            ((this.gsmJamming==null && other.getGsmJamming()==null) || 
             (this.gsmJamming!=null &&
              this.gsmJamming.equals(other.getGsmJamming()))) &&
            ((this.id==null && other.getId()==null) || 
             (this.id!=null &&
              this.id.equals(other.getId()))) &&
            ((this.input1==null && other.getInput1()==null) || 
             (this.input1!=null &&
              this.input1.equals(other.getInput1()))) &&
            ((this.latitude==null && other.getLatitude()==null) || 
             (this.latitude!=null &&
              this.latitude.equals(other.getLatitude()))) &&
            ((this.longitude==null && other.getLongitude()==null) || 
             (this.longitude!=null &&
              this.longitude.equals(other.getLongitude()))) &&
            ((this.moving==null && other.getMoving()==null) || 
             (this.moving!=null &&
              this.moving.equals(other.getMoving()))) &&
            ((this.output1==null && other.getOutput1()==null) || 
             (this.output1!=null &&
              this.output1.equals(other.getOutput1()))) &&
            ((this.panic==null && other.getPanic()==null) || 
             (this.panic!=null &&
              this.panic.equals(other.getPanic()))) &&
            ((this.serial==null && other.getSerial()==null) || 
             (this.serial!=null &&
              this.serial.equals(other.getSerial()))) &&
            ((this.snr==null && other.getSnr()==null) || 
             (this.snr!=null &&
              this.snr.equals(other.getSnr()))) &&
            ((this.speed==null && other.getSpeed()==null) || 
             (this.speed!=null &&
              this.speed.equals(other.getSpeed()))) &&
            ((this.svn==null && other.getSvn()==null) || 
             (this.svn!=null &&
              this.svn.equals(other.getSvn()))) &&
            ((this.temperature==null && other.getTemperature()==null) || 
             (this.temperature!=null &&
              this.temperature.equals(other.getTemperature()))) &&
            ((this.transmissionReason==null && other.getTransmissionReason()==null) || 
             (this.transmissionReason!=null &&
              this.transmissionReason.equals(other.getTransmissionReason())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAntiTheftStatus() != null) {
            _hashCode += getAntiTheftStatus().hashCode();
        }
        if (getBatteryCharging() != null) {
            _hashCode += getBatteryCharging().hashCode();
        }
        if (getBatteryFailure() != null) {
            _hashCode += getBatteryFailure().hashCode();
        }
        if (getCourse() != null) {
            _hashCode += getCourse().hashCode();
        }
        if (getCsq() != null) {
            _hashCode += getCsq().hashCode();
        }
        if (getDate() != null) {
            _hashCode += getDate().hashCode();
        }
        if (getExcessSpeed() != null) {
            _hashCode += getExcessSpeed().hashCode();
        }
        if (getGprsConnection() != null) {
            _hashCode += getGprsConnection().hashCode();
        }
        if (getGpsAntennaDisconnected() != null) {
            _hashCode += getGpsAntennaDisconnected().hashCode();
        }
        if (getGpsAntennaFailure() != null) {
            _hashCode += getGpsAntennaFailure().hashCode();
        }
        if (getGpsSignal() != null) {
            _hashCode += getGpsSignal().hashCode();
        }
        if (getGpsSleep() != null) {
            _hashCode += getGpsSleep().hashCode();
        }
        if (getGsmJamming() != null) {
            _hashCode += getGsmJamming().hashCode();
        }
        if (getId() != null) {
            _hashCode += getId().hashCode();
        }
        if (getInput1() != null) {
            _hashCode += getInput1().hashCode();
        }
        if (getLatitude() != null) {
            _hashCode += getLatitude().hashCode();
        }
        if (getLongitude() != null) {
            _hashCode += getLongitude().hashCode();
        }
        if (getMoving() != null) {
            _hashCode += getMoving().hashCode();
        }
        if (getOutput1() != null) {
            _hashCode += getOutput1().hashCode();
        }
        if (getPanic() != null) {
            _hashCode += getPanic().hashCode();
        }
        if (getSerial() != null) {
            _hashCode += getSerial().hashCode();
        }
        if (getSnr() != null) {
            _hashCode += getSnr().hashCode();
        }
        if (getSpeed() != null) {
            _hashCode += getSpeed().hashCode();
        }
        if (getSvn() != null) {
            _hashCode += getSvn().hashCode();
        }
        if (getTemperature() != null) {
            _hashCode += getTemperature().hashCode();
        }
        if (getTransmissionReason() != null) {
            _hashCode += getTransmissionReason().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Coordenadas.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://bean.tracker.easytracking.com.br/xsd", "Coordenadas"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("antiTheftStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("http://bean.tracker.easytracking.com.br/xsd", "antiTheftStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("batteryCharging");
        elemField.setXmlName(new javax.xml.namespace.QName("http://bean.tracker.easytracking.com.br/xsd", "batteryCharging"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("batteryFailure");
        elemField.setXmlName(new javax.xml.namespace.QName("http://bean.tracker.easytracking.com.br/xsd", "batteryFailure"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("course");
        elemField.setXmlName(new javax.xml.namespace.QName("http://bean.tracker.easytracking.com.br/xsd", "course"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("csq");
        elemField.setXmlName(new javax.xml.namespace.QName("http://bean.tracker.easytracking.com.br/xsd", "csq"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("date");
        elemField.setXmlName(new javax.xml.namespace.QName("http://bean.tracker.easytracking.com.br/xsd", "date"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("excessSpeed");
        elemField.setXmlName(new javax.xml.namespace.QName("http://bean.tracker.easytracking.com.br/xsd", "excessSpeed"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gprsConnection");
        elemField.setXmlName(new javax.xml.namespace.QName("http://bean.tracker.easytracking.com.br/xsd", "gprsConnection"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gpsAntennaDisconnected");
        elemField.setXmlName(new javax.xml.namespace.QName("http://bean.tracker.easytracking.com.br/xsd", "gpsAntennaDisconnected"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gpsAntennaFailure");
        elemField.setXmlName(new javax.xml.namespace.QName("http://bean.tracker.easytracking.com.br/xsd", "gpsAntennaFailure"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gpsSignal");
        elemField.setXmlName(new javax.xml.namespace.QName("http://bean.tracker.easytracking.com.br/xsd", "gpsSignal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gpsSleep");
        elemField.setXmlName(new javax.xml.namespace.QName("http://bean.tracker.easytracking.com.br/xsd", "gpsSleep"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gsmJamming");
        elemField.setXmlName(new javax.xml.namespace.QName("http://bean.tracker.easytracking.com.br/xsd", "gsmJamming"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://bean.tracker.easytracking.com.br/xsd", "id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("input1");
        elemField.setXmlName(new javax.xml.namespace.QName("http://bean.tracker.easytracking.com.br/xsd", "input1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("latitude");
        elemField.setXmlName(new javax.xml.namespace.QName("http://bean.tracker.easytracking.com.br/xsd", "latitude"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("longitude");
        elemField.setXmlName(new javax.xml.namespace.QName("http://bean.tracker.easytracking.com.br/xsd", "longitude"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("moving");
        elemField.setXmlName(new javax.xml.namespace.QName("http://bean.tracker.easytracking.com.br/xsd", "moving"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("output1");
        elemField.setXmlName(new javax.xml.namespace.QName("http://bean.tracker.easytracking.com.br/xsd", "output1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("panic");
        elemField.setXmlName(new javax.xml.namespace.QName("http://bean.tracker.easytracking.com.br/xsd", "panic"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("serial");
        elemField.setXmlName(new javax.xml.namespace.QName("http://bean.tracker.easytracking.com.br/xsd", "serial"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("snr");
        elemField.setXmlName(new javax.xml.namespace.QName("http://bean.tracker.easytracking.com.br/xsd", "snr"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("speed");
        elemField.setXmlName(new javax.xml.namespace.QName("http://bean.tracker.easytracking.com.br/xsd", "speed"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("svn");
        elemField.setXmlName(new javax.xml.namespace.QName("http://bean.tracker.easytracking.com.br/xsd", "svn"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("temperature");
        elemField.setXmlName(new javax.xml.namespace.QName("http://bean.tracker.easytracking.com.br/xsd", "temperature"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transmissionReason");
        elemField.setXmlName(new javax.xml.namespace.QName("http://bean.tracker.easytracking.com.br/xsd", "transmissionReason"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
