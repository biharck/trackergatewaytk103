package br.com.easytracking.tracker.position;

public class ReceiverPositionPortTypeProxy implements br.com.easytracking.tracker.position.ReceiverPositionPortType {
  private String _endpoint = null;
  private br.com.easytracking.tracker.position.ReceiverPositionPortType receiverPositionPortType = null;
  
  public ReceiverPositionPortTypeProxy() {
    _initReceiverPositionPortTypeProxy();
  }
  
  public ReceiverPositionPortTypeProxy(String endpoint) {
    _endpoint = endpoint;
    _initReceiverPositionPortTypeProxy();
  }
  
  private void _initReceiverPositionPortTypeProxy() {
    try {
      receiverPositionPortType = (new br.com.easytracking.tracker.position.ReceiverPositionLocator()).getReceiverPositionHttpSoap11Endpoint();
      if (receiverPositionPortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)receiverPositionPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)receiverPositionPortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (receiverPositionPortType != null)
      ((javax.xml.rpc.Stub)receiverPositionPortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public br.com.easytracking.tracker.position.ReceiverPositionPortType getReceiverPositionPortType() {
    if (receiverPositionPortType == null)
      _initReceiverPositionPortTypeProxy();
    return receiverPositionPortType;
  }
  
  public void receiver(br.com.easytracking.tracker.bean.xsd.Coordenadas[] coordenadas, java.lang.String token) throws java.rmi.RemoteException{
    if (receiverPositionPortType == null)
      _initReceiverPositionPortTypeProxy();
    receiverPositionPortType.receiver(coordenadas, token);
  }
  
  
}