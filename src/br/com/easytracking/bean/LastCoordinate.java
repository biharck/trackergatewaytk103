package br.com.easytracking.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="lastcoordinate")
public class LastCoordinate {
	
	private Integer id;
	private Integer last;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Integer getId() {
		return id;
	}
	public Integer getLast() {
		return last;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setLast(Integer last) {
		this.last = last;
	}
}
