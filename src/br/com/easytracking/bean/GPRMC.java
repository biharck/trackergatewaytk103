package br.com.easytracking.bean;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="gprmc")
public class GPRMC {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	private Timestamp date;
	private String imei;
	private String phone;
	private char satelliteFixStatus;
	private String latitudeDecimalDegrees;
	private char latitudeHemisphere;
	private String longitudeDecimalDegrees;
	private char longitudeHemisphere;
	private Float speed;
	private char gpsSignalIndicator;
	private String infotext;
	private String address;
	
	public Integer getId() {
		return id;
	}
	public Timestamp getDate() {
		return date;
	}
	public String getImei() {
		return imei;
	}
	public String getPhone() {
		return phone;
	}
	public char getSatelliteFixStatus() {
		return satelliteFixStatus;
	}
	public String getLatitudeDecimalDegrees() {
		return latitudeDecimalDegrees;
	}
	public char getLatitudeHemisphere() {
		return latitudeHemisphere;
	}
	public String getLongitudeDecimalDegrees() {
		return longitudeDecimalDegrees;
	}
	public char getLongitudeHemisphere() {
		return longitudeHemisphere;
	}
	public Float getSpeed() {
		return speed;
	}
	public char getGpsSignalIndicator() {
		return gpsSignalIndicator;
	}
	public String getInfotext() {
		return infotext;
	}
	public String getAddress() {
		return address;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public void setDate(Timestamp date) {
		this.date = date;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public void setSatelliteFixStatus(char satelliteFixStatus) {
		this.satelliteFixStatus = satelliteFixStatus;
	}
	public void setLatitudeDecimalDegrees(String latitudeDecimalDegrees) {
		this.latitudeDecimalDegrees = latitudeDecimalDegrees;
	}
	public void setLatitudeHemisphere(char latitudeHemisphere) {
		this.latitudeHemisphere = latitudeHemisphere;
	}
	public void setLongitudeDecimalDegrees(String longitudeDecimalDegrees) {
		this.longitudeDecimalDegrees = longitudeDecimalDegrees;
	}
	public void setLongitudeHemisphere(char longitudeHemisphere) {
		this.longitudeHemisphere = longitudeHemisphere;
	}
	public void setSpeed(Float speed) {
		this.speed = speed;
	}
	public void setGpsSignalIndicator(char gpsSignalIndicator) {
		this.gpsSignalIndicator = gpsSignalIndicator;
	}
	public void setInfotext(String infotext) {
		this.infotext = infotext;
	}
	public void setAddress(String address) {
		this.address = address;
	}
}
